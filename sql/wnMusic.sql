create table if not exists `wn_category`(
    `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '分类名称',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    `create_user` bigint DEFAULT NULL COMMENT '创建人',
    `update_user` bigint DEFAULT NULL COMMENT '修改人',
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_category_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='主页导航栏列表';

insert into `wn_category` values
    (1,'发现音乐',current_timestamp,current_timestamp,1,1),
    (2,'我的音乐',current_timestamp,current_timestamp,1,1),
    (3,'关注',current_timestamp,current_timestamp,1,1),
    (4,'商城',current_timestamp,current_timestamp,1,1),
    (5,'音乐人',current_timestamp,current_timestamp,1,1),
    (6,'云推歌',current_timestamp,current_timestamp,1,1);

create table if not exists `wn_category_child`(
    `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '分类名称',
    `parents` bigint not null COMMENT '父id',
    `path` varchar(200) not null comment '路径',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    `create_user` bigint DEFAULT NULL COMMENT '创建人',
    `update_user` bigint DEFAULT NULL COMMENT '修改人',
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_category_child_name` (`name`)
    ) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='主页导航栏子列表';

insert into `wn_category_child` values
    (1,'推荐',1,'',current_timestamp,current_timestamp,1,1),
    (2,'排行榜',1,'',current_timestamp,current_timestamp,1,1),
    (3,'歌单',1,'',current_timestamp,current_timestamp,1,1),
    (4,'主播电台',1,'',current_timestamp,current_timestamp,1,1),
    (5,'歌手',1,'',current_timestamp,current_timestamp,1,1),
    (6,'新碟商家',1,'',current_timestamp,current_timestamp,1,1);