package com.wn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WnCategoryChildDTO {

    private Long id;
    private String name;
    private Long parents;
    private String path;
}
