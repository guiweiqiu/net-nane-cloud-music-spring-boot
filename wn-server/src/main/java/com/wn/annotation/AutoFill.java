package com.wn.annotation;

import com.wn.enumeration.OperationType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解，用于标识某个方法要进行功能字段自动填充处理
 */
@Target(ElementType.METHOD)//标识该注解只能加载到方法上
@Retention(RetentionPolicy.RUNTIME)//运行时生效
public @interface AutoFill {
    //数据库操作类型：Update Insert
    OperationType value();
}
