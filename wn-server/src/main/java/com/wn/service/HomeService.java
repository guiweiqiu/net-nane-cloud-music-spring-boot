package com.wn.service;

import com.wn.dto.WnCategoryChildDTO;
import com.wn.dto.WnCategoryDTO;

import java.util.List;

public interface HomeService {
    /**
     * 查询导航菜单
     * @return
     */
    List<WnCategoryDTO> getCategoryList();

    /**
     * 查询子导航菜单
     * @param parents
     * @return
     */
    List<WnCategoryChildDTO> getCategoryChildList(Integer parents);
}
