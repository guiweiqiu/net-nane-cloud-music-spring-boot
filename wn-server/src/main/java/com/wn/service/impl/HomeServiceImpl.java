package com.wn.service.impl;

import com.wn.dto.WnCategoryChildDTO;
import com.wn.dto.WnCategoryDTO;
import com.wn.mapper.CategoryChildMapper;
import com.wn.mapper.CategoryMapper;
import com.wn.service.HomeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class HomeServiceImpl implements HomeService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private CategoryChildMapper categoryChildMapper;

    /**
     * 查询导航菜单
     * @return
     */
    @Override
    public List<WnCategoryDTO> getCategoryList() {
        List<WnCategoryDTO> wnCategoryDTOList = categoryMapper.list();
        return wnCategoryDTOList;
    }

    /**
     * 查询子导航菜单
     * @param parents
     * @return
     */
    @Override
    public List<WnCategoryChildDTO> getCategoryChildList(Integer parents) {
        List<WnCategoryChildDTO> wnCategoryChildDTOList = categoryChildMapper.listByParents(parents);
        return wnCategoryChildDTOList;
    }
}
