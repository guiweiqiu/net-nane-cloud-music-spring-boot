package com.wn.mapper;

import com.wn.dto.WnCategoryDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper {
    @Select("select * from wn_category")
    List<WnCategoryDTO> list();
}
