package com.wn.mapper;

import com.wn.dto.WnCategoryChildDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryChildMapper {
    @Select("select * from wn_category_child")
    List<WnCategoryChildDTO> list();

    @Select("select * from wn_category_child where parents = #{parents}")
    List<WnCategoryChildDTO> listByParents(Integer parents);
}
