package com.wn.aspect;

import com.wn.annotation.AutoFill;
import com.wn.constant.AutoFillConstant;
import com.wn.context.BaseContext;
import com.wn.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * 自定义切面类，实现公共字段自动填充逻辑
 */
@Aspect//声明这个是aop切面
@Component
@Slf4j
public class AutoFillAspect {
    /**
     * 切入点
     */
    //切入点注解,表示返回类型为 *,并拦截 mapper 层下的所有类和所有方法,匹配所有的参数类型(..)
    //annotation 表示拦截带有某个注解的方法,这里将所有带有 AutoFill注解的方法全部拦截
    @Pointcut("execution(* com.wn.mapper.*.*(..)) && @annotation(com.wn.annotation.AutoFill)")
    public void autoFillPointCut(){}

    /**
     * 前置通知，在通知中进行公共字段的赋值
     */
    @Before("autoFillPointCut()")
    public void autoFill(JoinPoint joinPoint){
        log.info("开始公告字段填充");

        //获取当前被拦截的方法的数据库类型
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();//获取方法签名
        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);//获取方法上的注解
        OperationType operationType = autoFill.value();//获取注解的值,也就是数据库操作类型

        //获取当前被拦截方法的参数 -》 实体对象
        Object[] args = joinPoint.getArgs();
        if(args == null || args.length == 0){
            return;
        }

        Object entity = args[0];

        //准备赋值的数据
        LocalDateTime now = LocalDateTime.now();
        Long currentId = BaseContext.getCurrentId();

        //按照操作类型的不同,对实体对象的公共属性赋值,使用反射进行赋值
        if(operationType == OperationType.INSERT){
            //为四个公告字段赋值
            try{
                Method setCreateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
                Method setCreateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
                Method setUpdateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

                //通过反射赋值
                setCreateTime.invoke(entity,now);
                setCreateUser.invoke(entity,currentId);
                setUpdateTime.invoke(entity,now);
                setUpdateUser.invoke(entity,currentId);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }else if(operationType == OperationType.UPDATE){
            //为两个公共字段赋值
            try{
                Method setUpdateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);
                setUpdateTime.invoke(entity,now);
                setUpdateUser.invoke(entity,currentId);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
