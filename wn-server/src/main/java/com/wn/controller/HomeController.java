package com.wn.controller;

import com.wn.dto.WnCategoryChildDTO;
import com.wn.dto.WnCategoryDTO;
import com.wn.result.Result;
import com.wn.service.HomeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
@Slf4j
@CrossOrigin
public class HomeController {

    @Autowired
    private HomeService homeService;

    @GetMapping("/category/head")
    public Result getCategoryList(){
        List<WnCategoryDTO> wnCategoryDTOList = homeService.getCategoryList();
        log.info("category : {}",wnCategoryDTOList);
        return Result.success(wnCategoryDTOList);
    }

    @GetMapping("/category/child/{parents}")
    public Result getCategoryChildList(@PathVariable Integer parents){
        List<WnCategoryChildDTO> wnCategoryChildDTOList = homeService.getCategoryChildList(parents);
        return Result.success(wnCategoryChildDTOList);
    }

}
